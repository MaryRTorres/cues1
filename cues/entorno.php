<html>
<head>
	<title>Cues</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<header>
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        
      </div>
      <div class="col-md-8 logoCues">
          <img class="arbol" src="icon/arbol.png">
          <img class="logo" src="img/logo.png"> 
      </div>
    </div>
  </div>
</div>
</header>
<body data-spy="scroll" data-target=".navbar" data-offset="80">
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li><a href="index.php">INICIO</a></li>
      <li><a href="clusters.php">CLÚSTERS</a></li>
      <li class="active"><a href="entorno.php">ENTORNO</a></li>
      <li><a href="galeria.php">GALERÍA</a></li>
      <li><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</nav>

<!-- Fondo clusters -->
<section class="cues_entorno">
  <div class="contactBar">
    <a href="https://www.facebook.com/Loscues/?fref=ts"><img src="icon/facebook.png"></a>
      <a href="contacto.php">
      <img src="icon/correo-simbolo-de-sobre-negro copia.png"></a>
      <a href="contacto.php">
      <img src="icon/marcador-de-posicion-3 copia.png"></a>
      <a href="contacto.php">
      <img src="icon/receptor-de-telefono copia.png"></a>
  </div>
  <div class="container cues_entorno_container">
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
</section>
<!-- /. Fondo clusters -->
<!-- Information -->
<section class="cues_entorno_ubicacion">
  <div class="container cues_entorUbicacion_container">
    <div class="row">
      <div class="cues_ubicacionImg col-md-12">
        <div class="col-md-6 planoImg">
          <img src="img/PLANO.jpg">
        </div>
        <div class="col-md-6 planoText">
          <img src="img/logo.png">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_entorno_acceso">
  <div class="container cues_entorno_acceso_container">
    <div class="row">
      <div class="cues_entorno_acceso_slider col-md-12">
        <div class="col-md-8 cues_entornoSlider">
          <h1>CASA CLUB</h1>
          <div id="myCarousel" class="carousel slide carousel-fade entorno_slider" data-ride="carousel">

           <!-- Wrapper for slides -->
           <div class="carousel-inner" role="listbox">
             <div class="item active">
               <img src="img/04_Club_amb.png">
               <div class="carousel-caption">
               </div>
             </div>

             <div class="item">
               <img src="img/07_Club.png">
               <div class="carousel-caption">
               </div>
             </div>
             <div class="item">
               <img src="img/01_Club_amb.png" alt="Flower">
               <div class="carousel-caption">
               </div>
             </div>
           </div>

           <!-- Left and right controls -->
           <a class="left left_entorno carousel-control" href="#myCarousel" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
           </a>
           <a class="right right_entorno carousel-control" href="#myCarousel" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
           </a>
          </div>
        </div>
        <div class="col-md-4 cues_entornoText">
          <ul>
            <li>ALBERCAS</li>
            <li>CALEFACCIÓN SOLAR</li>
            <li>CANCHAS MULTIFUNCIONALES</li>
            <li>GIMNACIO</li>
            <li>ESTACIONAMIENTO</li>
            <li>ÁREA DE ASADORES</li>
            <li>HACIENDA PARA EVENTOS</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_entorno_eventos">
  <div class="container cues_entorno_eventos_container">
    <div class="row">
      <div class="col-md-12 cues_eventos">
        <div class="col-md-6 cues_eventosImg">
          <img src="img/IMG_0803.JPG">
        </div>
        <div class="col-md-6 cues_eventosText">
          <h1>HACIENDA PARA EVENTOS</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_entorno_circuito">
  <div class="container cues_entorno_circuito_container">
    <div class="row">
      <div class="col-md-12">
        <h1>CIRCUITO COMERCIAL</h1>
        <img src="img/circuito.jpg">
      </div>
    </div>
  </div>
</section>
<section class="cues_entorno_slider2">
  <div class="container cues_entorno_slider2_container">
    <div class="row">
      <div class="col-md-12">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="col-md-11 cues_entornoSlider">
          <div id="myCarousel" class="carousel slide carousel-fade entorno_slider" data-ride="carousel">

           <!-- Wrapper for slides -->
           <div class="carousel-inner" role="listbox">
             <div class="item active">
               <img src="img/Fotolia_122956719_M.jpg">
               <div class="carousel-caption">
               </div>
             </div>

             <div class="item">
               <img src="img/Fotolia_136755886_L.jpg">
               <div class="carousel-caption">
               </div>
             </div>
             <div class="item">
               <img src="img/shutterstock_129527819.jpg" alt="Flower">
               <div class="carousel-caption">
               </div>
             </div>
           </div>

           <!-- Left and right controls -->
           <a class="left left_entorno carousel-control" href="#myCarousel" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
           </a>
           <a class="right right_entorno carousel-control" href="#myCarousel" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
           </a>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<section class="cues_contact">
  <div class="container cues_contact_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1 class="cues_contact_title">CONTACTO</h1>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
            <a href="https://www.facebook.com/Loscues/?fref=ts"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>