<html>
<head>
	<title>Cues</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<header>
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        
      </div>
      <div class="col-md-8 logoCues">
          <img class="arbol" src="icon/arbol.png">
          <img class="logo" src="img/logo.png"> 
      </div>
    </div>
  </div>
</div>
</header>
<body data-spy="scroll" data-target=".navbar" data-offset="80">
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">INICIO</a></li>
      <li><a href="clusters.php">CLÚSTERS</a></li>
      <li><a href="entorno.php">AMENIDADES</a></li>
      <li><a href="galeria.php">GALERÍA</a></li>
      <li><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</nav>

<!-- Slider -->
<section class="cues_slider">
    <div class="contactBar">
      <a href="https://www.facebook.com/Loscues/?fref=ts"><img src="icon/facebook.png"></a>
      <a href="contacto.php">
      <img src="icon/correo-simbolo-de-sobre-negro copia.png"></a>
      <a href="contacto.php">
      <img src="icon/marcador-de-posicion-3 copia.png"></a>
      <a href="contacto.php">
      <img src="icon/receptor-de-telefono copia.png"></a>
    </div>
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
     <!-- Indicators -->
     <ol class="carousel-indicators">
       <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
       <li data-target="#myCarousel" data-slide-to="1"></li>
       <li data-target="#myCarousel" data-slide-to="2"></li>
     </ol>

     <!-- Wrapper for slides -->
     <div class="carousel-inner" role="listbox">
       <div class="item active">
         <img src="img/Fotolia_98278469_M.jpg">
         <div class="carousel-caption">
         </div>
       </div>

       <div class="item">
         <img src="img/Fotolia_117205642_M.jpg">
         <div class="carousel-caption">
         </div>
       </div>
       <div class="item">
         <img src="img/Fotolia_122956719_M.jpg" alt="Flower">
         <div class="carousel-caption">
         </div>
       </div>
     </div>

     <!-- Left and right controls -->
     <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
       <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
       <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
    </div>
  </section>
<!-- /. Slider -->
<!-- Information -->
<section class="cues_information">
  <div class="container cues_information_container">
    <div class="row">
      <div class="col-md-12 cues_information_col">
        <div class="col-md-6">
          <h1>¡ZONA DE GRAN</br>PLUSVALÍA EN QUERÉTARO!</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="col-md-6 cues_information_img">
          <img src="img/information.png">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_gallery">
  <div class="container cues_gallery_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4">
          <img src="img/01_Club_amb.png">
          <h2>CASA CLUB</h2>
          <input type="button" name="" value="Ver más" class="verMas">
        </div>
        <div class="col-md-4">
          <img src="img/02_Club.png">
          <h2>ACCESOS CONTROLADOS</h2>
          <input type="button" name="" value="Ver más" class="verMas">
        </div>
        <div class="col-md-4">
          <img src="img/08_Club.png">
          <h2>HACIENDA PARA EVENTOS </h2>
          <input type="button" name="" value="Ver más" class="verMas">
        </div>
      </div>
    </div>
  </div>
</section>
<section>
<a href="entorno.php">
  <div class="row mapa">
      <div class="col-md-12 mapa_container">
          <img src="img/MAPA.png"> 
      </div>
    </div></a>
    <div class="row cues_promo">
      <div class="col-md-12 cues_promo_container">
        <h1>¡PREGUNTA POR NUESTRAS</br> PROMOCIONES!</h1>
      </div>
    </div>
</section>
<section class="cues_contact">
  <div class="container cues_contact_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1>CONTACTO</h1>
          <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
            <a href="https://www.facebook.com/Loscues/?fref=ts"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <h2>Formulario</h2>
            <form action="/action_page.php">
              <div class="form-group">
                <input type="text" class="form-control" id="name" placeholder="*Nombre" name="name">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="correo" placeholder="*Correo" name="correo">
              </div>
              <div class="form-group">
                <input type="tel" class="form-control" id="phone" placeholder="*Teléfono" name="phone">
              </div>
              <div class="form-group">
                <textarea class="form-control" rows="5" id="comment" placeholder="*Comentario"></textarea>
              </div>
              <button type="submit" class="btn btn-default">Envia mensaje</button>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>