<html>
<head>
	<title>Cues</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<header>
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        
      </div>
      <div class="col-md-8 logoCues">
          <img class="arbol" src="icon/arbol.png">
          <img class="logo" src="img/logo.png"> 
      </div>
    </div>
  </div>
</div>
</header>
<body data-spy="scroll" data-target=".navbar" data-offset="80">
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li><a href="index.php">INICIO</a></li>
      <li class="active"><a href="clusters.php">CLÚSTERS</a></li>
      <li><a href="entorno.php">ENTORNO</a></li>
      <li><a href="galeria.php">GALERÍA</a></li>
      <li><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</nav>

<!-- Fondo clusters -->
<section class="cues_clusters">
  <div class="contactBar">
    <a href="https://www.facebook.com/Loscues/?fref=ts"><img src="icon/facebook.png"></a>
      <a href="contacto.php">
      <img src="icon/correo-simbolo-de-sobre-negro copia.png"></a>
      <a href="contacto.php">
      <img src="icon/marcador-de-posicion-3 copia.png"></a>
      <a href="contacto.php">
      <img src="icon/receptor-de-telefono copia.png"></a>
  </div>
  <div class="container cues_clusters_container">
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
</section>
<!-- /. Fondo clusters -->
<!-- Information -->
<section class="cues_clustersinfo">
  <div class="container cues_clustersinfo_container">
    <div class="row">
      <div class="col-md-12 cues_clustersinfo_col">
        <h1>ENTRADA A LOS CLUSTERS CON CASETA DE VIGILANCIA</h1>
        <div class="col-md-4">
          <h2>Clústers</h2>
        </div>
        <div class="col-md-8">
          <p>Todos los clusters cuentan con albercas, gimnasios, canchas multifuncionales, acceso controlado en todos los clusters y circuito cerrado las 24 horas los 365 días del año.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_clusters_gallery">
  <div class="container cues_clusters_gallery_container">
    <div class="row">
      <div class="cues_clustersimg col-md-12">
        <h1>CLUSTERS</h1>
        <img src="img/Cuez_altasindegradado-7.png">
      </div>
    </div>
  </div>
</section>
<section class="cues_etapas">
  <div class="container cues_etapas_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1>PRIMER ETAPA</h1>
          <h2>REAL DE GANTE</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <h2>REAL DE PORTOLA</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <h2>REAL DE SERRA</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div class="col-md-6 lotesImg">
          <img src="img/LOTES/IMG_0897.JPG">
          <img src="img/LOTES/IMG_0898.JPG">
          <img src="img/LOTES/IMG_0899.JPG">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_contact">
  <div class="container cues_contact_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1 class="cues_contact_title">CONTACTO</h1>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
            <a href="https://www.facebook.com/Loscues/?fref=ts"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>