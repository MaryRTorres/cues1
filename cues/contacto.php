<html>
<head>
	<title>Cues</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<header>
  <div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        
      </div>
      <div class="col-md-8 logoCues">
          <img class="arbol" src="icon/arbol.png">
          <img class="logo" src="img/logo.png"> 
      </div>
    </div>
  </div>
</div>
</header>
<body data-spy="scroll" data-target=".navbar" data-offset="80">
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li><a href="index.php">INICIO</a></li>
      <li><a href="clusters.php">CLÚSTERS</a></li>
      <li><a href="entorno.php">ENTORNO</a></li>
      <li><a href="galeria.php">GALERÍA</a></li>
      <li class="active"><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</nav>

<!-- Fondo clusters -->
<section class="cues_contacto">
  <div class="container cues_clusters_container">
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
</section>
<!-- /. Fondo clusters -->
<section class="cues_contact2">
  <div class="container cues_contact_container2">
    <div class="row">
      <div class="col-md-12 cues_contactCol">
        <div class="col-md-6">
          <h1>CONTACTO</h1>
          <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <h2>Formulario</h2>
            <form action="/action_page.php">
              <div class="form-group">
                <input type="text" class="form-control" id="name" placeholder="*Nombre" name="name">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="correo" placeholder="*Correo" name="correo">
              </div>
              <div class="form-group">
                <input type="tel" class="form-control" id="phone" placeholder="*Teléfono" name="phone">
              </div>
              <div class="form-group">
                <textarea class="form-control" rows="5" id="comment" placeholder="*Comentario"></textarea>
              </div>
              <button type="submit" class="btn btn-default">Envia mensaje</button>
            </form>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_direcciones">
  <div class="container cues_direcciones_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <img src="img/arbol.png">
        </div>
        <div class="col-md-6">
          <ul>
            <li>
              a 10 min. del centro de Querétato
            </li>
            <li>Zona de gran prusvalia en Querétaro</li>
            <li>Vive en contacto con la naturaleza</li>
            <li>Circuito exterior con comercios</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Information -->
<section>
  <div class="row mapa">
      <div class="col-md-12 mapa_container"> 
          <iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d14933.595990662743!2d-100.4003394!3d20.65334015!3m2!1i1024!2i768!4f13.1!2m1!1smapa+aguacates+castillo!5e0!3m2!1ses!2smx!4v1486500529419" width="1518" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
</section>
<section class="cues_contact">
  <div class="container cues_contact_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1 class="cues_contact_title">CONTACTO</h1>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
            <a href="https://www.facebook.com/Loscues/?fref=ts"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>