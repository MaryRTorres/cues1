<html>
<head>
	<title>Cues</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<header>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-4">
        
      </div>
      <div class="col-md-8 logoCues">
          <img class="arbol" src="icon/arbol.png">
          <img class="logo" src="img/logo.png"> 
      </div>
    </div>
  </div>
</div>
</header>
<body data-spy="scroll" data-target=".navbar" data-offset="80">
<nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <ul class="nav navbar-nav">
      <li><a href="index.php">INICIO</a></li>
      <li><a href="clusters.php">CLÚSTERS</a></li>
      <li><a href="entorno.php">ENTORNO</a></li>
      <li class="active"><a href="galeria.php">GALERÍA</a></li>
      <li><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</nav>

<!-- Fondo clusters -->
<section class="cues_galeria">
  <div class="contactBar">
    <a href="https://www.facebook.com/Loscues/?fref=ts"><img src="icon/facebook.png"></a>
      <a href="contacto.php">
      <img src="icon/correo-simbolo-de-sobre-negro copia.png"></a>
      <a href="contacto.php">
      <img src="icon/marcador-de-posicion-3 copia.png"></a>
      <a href="contacto.php">
      <img src="icon/receptor-de-telefono copia.png"></a>
  </div>
  <div class="container cues_clusters_container">
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
</section>
<h1 class="titleGaleria">GALERIA</h1>
<!-- /. Fondo clusters -->
<section class="cues_galeriaImg">
  <div class="container cues_galeria_container">
    <div class="row">
      <div class="col-md-12">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        <div class="col-md-6 galeria1">
          <img src="img/02_EXT_LAGO_amb.png">
          <img src="img/03_EXT_LAGO_amb.png">
          <img src="img/04_Club_amb.png">
          <img src="img/07_Club.png">
        </div>
        <div class="col-md-6 galeria2">
          <img src="img/IMG_0762.PNG">
          <img src="img/IMG_0763.PNG">
          <img src="img/IMG_0764.PNG">
          <img src="img/IMG_0765.PNG">
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cues_contact">
  <div class="container cues_contact_container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6">
          <h1 class="cues_contact_title">CONTACTO</h1>
        </div>
        <div class="col-md-6">
          <div class="cues_form">
            <div class="col-md-2 cues_icon">
            <i class="fa fa-phone fa-3x" aria-hidden="true"></i>
            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
            <a href="https://www.facebook.com/Loscues/?fref=ts"><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 cues_text_cont">
            <p>442 454 55 26</p>
            <p>info@realdeloscues.com</p>
            <p>Plaza Q7001
            Blvd. Bernardo Quintana 7001
            2do piso Local 205
            Col Centro Sur
            Queretaro, Qro
            </p>
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>